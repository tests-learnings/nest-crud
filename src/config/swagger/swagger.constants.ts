import { DocumentBuilder } from '@nestjs/swagger';
import { ENVIRONMENT } from '../config.constants';

export const SWAGGER_TITLE: string = ENVIRONMENT.SWAGGER_TITLE;
export const SWAGGER_DESCRIPTION: string = ENVIRONMENT.SWAGGER_DESCRIPTION;
export const SWAGGER_VERSION: string = ENVIRONMENT.SWAGGER_VERSION;
export const SWAGGER_TAG: string = ENVIRONMENT.SWAGGER_TAG;

export const SWAGGER_PATH: string = ENVIRONMENT.SWAGGER_PATH;


export const SWAGGER_CONFIG = new DocumentBuilder()
.setTitle(SWAGGER_TITLE)
.setDescription(SWAGGER_DESCRIPTION)
.setVersion(SWAGGER_VERSION)
.addTag(SWAGGER_TAG)
.build();