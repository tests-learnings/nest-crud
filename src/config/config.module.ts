import { Module } from '@nestjs/common';
import { AppModule } from './app/app.module';
import { DatabaseModule } from './database/database.module';
import { AuthModule } from './auth/auth.module';
import { EnvironmentModule } from './environment/environment.module';
import { SwaggerModule } from './swagger/swagger.module';

@Module({
  imports: [AppModule, DatabaseModule, AuthModule, EnvironmentModule, SwaggerModule]
})
export class ConfigModule {}
