import { ENVIRONMENT } from '../config.constants';

export const JWT_SECRET: string = ENVIRONMENT.JWT_SECRET;
export const JWT_LIFETIME: string = ENVIRONMENT.JWT_LIFETIME;
