import { ENVIRONMENT } from '../config.constants';

export const DB_HOST: string = ENVIRONMENT.DB_HOST;
export const DB_PORT: number = Number(ENVIRONMENT.DB_PORT);
export const DB_DATABASE: string = ENVIRONMENT.DB_DATABASE;
export const DB_USERNAME: string = ENVIRONMENT.DB_USERNAME;
export const DB_PASSWORD: string = ENVIRONMENT.DB_PASSWORD;