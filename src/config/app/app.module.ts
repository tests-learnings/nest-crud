import { Logger, Module } from '@nestjs/common';
import { APP_NAME, APP_PORT } from './app.constants';

@Module({
  imports: [],
})
export class AppModule {
  private readonly logger = new Logger('InstanceLoader');

  constructor() {
    this.logger.log(`${APP_NAME} RUNNING ON PORT: ${APP_PORT}`)
  }

}
