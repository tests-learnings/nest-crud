import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { IsString, MaxLength, Validate, IsEnum } from 'class-validator';
import { UserType } from '../entities/user.entity';
import { IsUsernameUnique } from './validators/is-username-unique.validator';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  @MaxLength(50, {
    message: 'Username is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  //@Validate(IsUsernameUnique)
  username: string;

  @IsString()
  @MaxLength(50, {
    message: 'Password is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  password: string;

  @IsEnum(UserType)
  type: UserType;
}
