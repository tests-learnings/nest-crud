import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Repository } from 'typeorm';
import { UserEntity } from '../../entities/user.entity';

@ValidatorConstraint({ name: 'IsUsernameUnique', async: true })
@Injectable()
export class IsUsernameUnique implements ValidatorConstraintInterface {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    ) {}

  async validate(value: string): Promise<boolean> {
    const user: UserEntity = await this.userRepository.findOne({ where: { username: value } });
    return !user;
  }
}
