import { IsEnum, IsNotEmpty, IsString, MaxLength, Validate } from 'class-validator';
import { UserType } from '../entities/user.entity';
import { IsUsernameUnique } from './validators/is-username-unique.validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    example: 'username@example.com',
  })
  @IsNotEmpty({ message: 'Usename is required' })
  @IsString()
  @MaxLength(50, {
    message: 'Username is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  //@Validate(IsUsernameUnique, { message: 'Username exists' })
  username: string;

  @ApiProperty({
    example: '123456'
  })
  @IsNotEmpty({ message: 'Password is required' })
  @IsString()
  @MaxLength(50, {
    message: 'Password is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  password: string;

  @ApiProperty({
    enum: UserType,
    example: UserType.ADMIN,
  })
  @IsNotEmpty({ message: 'User type is required' })
  @IsEnum(UserType, {
    message: 'User type must be ADMIN or CLIENT',
  })
  type: UserType;
}
