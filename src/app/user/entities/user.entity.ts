import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

export enum UserType {
  ADMIN = 'ADMIN',
  CLIENT = 'CLIENT',
};

@Entity({
  name: 'users',
})
export class UserEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;
  
  @Column({ default: UserType.ADMIN })
  type: UserType;

}
