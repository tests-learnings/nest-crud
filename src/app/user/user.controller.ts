import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Controller('user/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(
    @Res() res,
    @Body() createUserDto: CreateUserDto
  ) {
    const user: UserEntity = await this.userService.create(createUserDto);
    delete user.password;
    return res.status(HttpStatus.CREATED).json(user);
  }

  @Get()
  async findAll(
    @Res() res,
  ) {
    const users: UserEntity[] = await (await this.userService.findAll());
    users.forEach(item => delete item.password);
    const response = res.status(HttpStatus.OK).json(users);
    return response;
  }

  @Get(':id')
  async findOne(
    @Res() res,
    @Param('id') id: string,
  ) {
    const user: UserEntity = await this.userService.findOne(+id);
    const response = res.status(HttpStatus.OK).json(user);
    return response;
  }

  @Patch(':id')
  async update(
    @Res() res,
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    const user: UserEntity = await this.userService.update(+id, updateUserDto);
    const response = res.status(HttpStatus.OK).json(user);
    return response;
  }

  @Delete(':id')
  async remove(
    @Res() res,
    @Param('id') id: string,
  ): Promise<any> {
    await this.userService.remove(+id);
    const response = res.status(HttpStatus.NO_CONTENT);
    return response;
  }
}
