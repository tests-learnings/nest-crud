import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { APP_SALT } from 'src/config/app/app.constants';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserEntity> {
    const passwordHased = await bcrypt.hash(createUserDto.password, APP_SALT);
    const user = await this.userRepository.save<any>({ ...createUserDto, password: passwordHased });
    return user;
  }

  async findAll(): Promise<UserEntity[]> {
    const users: UserEntity[] = await this.userRepository.find();
    return users;
  }

  async findOne(id: number): Promise<UserEntity> {
    const user: UserEntity = await this.userRepository.findOne({
      where: {
        id: id,
      },
    });
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<UserEntity> {
    const user: UserEntity = await this.userRepository.findOne({
      where: { id },
    });
    await this.userRepository.save({ ...updateUserDto });
    return user;
  }

  async remove(id: number): Promise<void> {
    const user: UserEntity = await this.findOne(id);
    await this.userRepository.delete(id);
  }
}
