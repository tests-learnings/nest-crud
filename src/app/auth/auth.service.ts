import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { LoginAuthDto } from './dto/login-auth.dto';
import { UserEntity } from '../user/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService
    ) {}
  
  async login(loginAuthDto: LoginAuthDto): Promise<any> {    
    const username: string = loginAuthDto.username;
    const user: UserEntity = await this.userRepository.findOne({
      where: {
        username,
      },
    });
    if (!user) {
      throw new BadRequestException('Credentials error', { cause: new Error(), description: 'User does not exists' });
    }

    const isValid: boolean = await bcrypt.compare(loginAuthDto.password, user.password);
    if (!isValid) {
      throw new BadRequestException('Credentials error', { cause: new Error(), description: 'Invalid password' });
    }
    const payload = { username: user.username };
    const response: any = { accessToken: this.jwtService.sign(payload) };
    return response;
  }
}
