import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { UserEntity } from '../user/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule as AuthConfigModule } from 'src/config/auth/auth.module';
import { JwtStrategy } from 'src/config/auth/jwt.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]), 
    UserModule,
    AuthConfigModule
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
