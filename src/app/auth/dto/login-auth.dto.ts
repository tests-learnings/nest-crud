import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class LoginAuthDto {
  @ApiProperty({
    example: 'username@example.com',
  })
  @IsNotEmpty({ message: 'Usename is required' })
  @IsString()
  @MaxLength(50, {
    message: 'Username is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  //@Validate(IsUsernameExists, { message: 'Username doesnt exists' })
  username: string;

  @ApiProperty({
    example: '123456',
  })
  @IsNotEmpty({ message: 'Password is required' })
  @IsString()
  @MaxLength(50, {
    message: 'Password is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  password: string;
}
