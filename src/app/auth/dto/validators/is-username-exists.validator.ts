import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { UserEntity } from 'src/app/user/entities/user.entity';
import { Repository } from 'typeorm';

@ValidatorConstraint({ name: 'IsUsernameExists', async: true })
@Injectable()
export class IsUsernameExists implements ValidatorConstraintInterface {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    ) {}

  async validate(value: string): Promise<boolean> {
    const user: UserEntity = await this.userRepository.findOne({ where: { username: value } });
    return !!user;
  }
}
