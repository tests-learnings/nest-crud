import { Body, Controller, Get, HttpCode, HttpStatus, Post, Request, Res, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/config/auth/jwt-auth.guard';
import { LoginAuthDto } from './dto/login-auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(
    @Res() res,
    @Body() loginAuthDto: LoginAuthDto,
  ): Promise<any> {
    const jwt: any = await this.authService.login(loginAuthDto);
    return res.status(HttpStatus.OK).json(jwt);
  }

  @UseGuards(JwtAuthGuard)
  @Get('authenticate')
  async authenticate(@Request() req): Promise<any> {
    const user = req.user;
    return user;
  }
}
